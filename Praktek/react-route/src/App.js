import logo from './logo.svg';
import './App.css';
// Import Router
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
          { /* Routing */ }
          <Router>
            <ul>
              <li>
                { /* Mengarahkan Route */ }
                <Link to="/home"> Home </Link>
              </li>
              <li>
                <Link to="/about"> About </Link>
              </li>
              <li>
                <Link to="/users"> Users </Link>
              </li>
            </ul>

            { /* Setting Path */ }
            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/users">
                <Users />
              </Route>
              <Route path="/home">
                <Home />
              </Route>
            </Switch>
          </Router>
        </div>
      </header>
    </div>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}


export default App;
