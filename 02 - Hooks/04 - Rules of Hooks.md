Hooks merupakan fungsi JavaScript yang memberlakukan dua aturan tambahan:

## Panggil Hook sesuai urutan pembuatannya 

React mengetahui state mana yang sesuai dengan yang dipanggil melalui urutan render hook tersebut. Selama urutan panggilan-panggilan Hook sama dalam setiap render, React dapat mengasosiasikan beberapa state lokal didalamnya

Misal :

```jsx
    function Form() {
        // 1. Menggunakan state variabel name
        const [name, setName] = useState('Mary');

        // 2. Menggunakan sebuah effect untuk mengukuhkan form
        useEffect(function persistForm() {
            localStorage.setItem('formData', name);
        });

        // 3. Menggunakan state variabel surname
        const [surname, setSurname] = useState('Poppins');

        // 4. Menggunakan sebuah effect untuk meng-update title
        useEffect(function updateTitle() {
            document.title = name + ' ' + surname;
        });
    // ...
    }
```

Pemanggilannya :

```jsx
    // Render pertama
    // ------------
    useState('Mary')           // 1. Inisialisasi state variabel name dengan 'Mary'
    useEffect(persistForm)     // 2. Tambahkan sebuah effect untuk mengukuhkan form
    useState('Poppins')        // 3. Inisialisasi state variabel surname dengan 'Poppins'
    useEffect(updateTitle)     // 4. Tambahkan sebuah effect untuk meng-update title
```

## Hanya panggil Hooks pada tingkat teratas

Lalu bagaimana jika kita meletakan `useEffect` dialam condition?. Misal :

```jsx
  // 🔴 Kita melanggar aturan pertama dengan menggunakan Hook di dalam sebuah condition
  if (name !== '') {
    useEffect(function persistForm() {
      localStorage.setItem('formData', name);
    });
  }
```

Condition diatas akan menyebabkan error karena ada hook yang dilangkahi. 

```jsx
    useState('Mary')           // 1. Baca state variabel name (argument diabaikan)
    // useEffect(persistForm)  // 🔴 Hook ini dilangkahi!
    useState('Poppins')        // 🔴 2 (sebelumnya 3). Gagal membaca state variabel surname
    useEffect(updateTitle)     // 🔴 3 (sebelumnya 4). Gagal mengganti effect
```

React tidak akan mengetahui apa yang harus dikembalikan pada saat panggilan Hook useState kedua. React mengharapkan bahwa panggilan Hook kedua di komponen ini sesuai dengan effect persistForm, seperti render sebelumnya, tetapi tidak lagi. Sejak saat itu, setiap panggilan Hook selanjutnya setelah yang kita langkahi juga akan bergeser satu, mengakibatkan bugs.

Itu sebabnya jangan panggil Hooks di dalam perulangan, kondisi, atau fungsi bertingkat. Tapi gunakan perulangan, kondisi dll didalam hooks tersebut sehingga hooks tetap akan dieksekusi, apapun isi argumentnya.

```jsx
  useEffect(function persistForm() {
    // 👍 Melewati tanpa melanggar aturan
    if (name !== '') {
      localStorage.setItem('formData', name);
    }
  });
```

## Hanya panggil Hooks dari komponen fungsi React

Jangan memanggil Hooks dari fungsi-fungsi JavaScript biasa. Tapi panggil hook dari :

1. komponen-komponen fungsi React
2. custom Hooks


