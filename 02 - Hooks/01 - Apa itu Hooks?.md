Hooks adalah fitur baru React yang hadir sejak versi 16.8. Dengan Hooks kita dapat menggunakan state dan fitur React yang lain tanpa perlu menulis sebuah kelas baru (Menggunakan function).

Contoh :

```jsx
import React, { useState } from 'react';

function Example() {
  // Mendeklarasikan variabel state baru, yaitu "count"
  const [count, setCount] = useState(0);

  return (
    <div>
      <p>Anda mengklik {count} kali</p>
      <button onClick={() => setCount(count + 1)}>
        Klik aku
      </button>
    </div>
  );
}
```

# Kelebihan Hooks

1. Bersifat opsional

> Kita bisa menggunakan cara lama (class) jika tetap nyaman dan malas migrasi :v 

2. Ada untuk meminimalisasi kode tanpa menghilangkan konsep dasar React

> Malahan, Hooks memberikan API yang lebih langsung ke konsep-konsep React yang Anda ketahui: props, state, context, refs, dan lifecycle. 

1. 100% backwards-compatible